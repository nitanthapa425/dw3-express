import { HttpStatus } from "../config/constant.js";
import errorResponse from "../helper/errorResponse.js";
import successResponse from "../helper/successResponse.js";
import { User } from "../schema/model.js";
import expressAsyncHandler from "express-async-handler";
import { sendMail } from "../utils/sendMail.js";

export let createUser = expressAsyncHandler(async (req, res, next) => {
  let result = await User.create(req.body);

  await sendMail({
    from: '"Nitan Thapa" <uniquekc425@gmail.com>',
    to: [
      "nitanthapa425@gmail.com",
      "swetadhakal100@gmail.com",
      "manishkdka12345@gmail.com",
    ],
    subject: "this is subject",
    html: `<h1>Hello World<h1>`,
  });

  console.log("email has been sent successfully.");

  successResponse(res, HttpStatus.CREATED, "User created successfully", result);
});

export let readUserDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await User.findById(req.params.id);
  successResponse(res, HttpStatus.OK, "Read User details successfully", result);
});

//get all
//update details (id)
//delete (id)

export let readAllUser = expressAsyncHandler(async (req, res, next) => {
  try {
    // let result = await User.find({});

    //object  which must have name:"nitan"

    // let result = await User.find({ name: "nitan" }); //perform searching
    // let result = await User.find({ name: "nitan", age: "29" });
    // let result = await User.find({ age: 33 });
    // let result = await User.find({ age: { $gt: 33 } });
    // let result = await User.find({ age: { $gte: 33 } });
    // let result = await User.find({ age: { $lt: 33 } });
    // let result = await User.find({ age: { $lte: 33 } });
    // let result = await User.find({ age: { $ne: 33 } });
    // let result = await User.find({ name: { $in: ["zzzz", "yyyy"] } });

    //object search
    // let result = await User.find({ "location.country": "china" }); //alway wrap key by double quotes of object

    // array search
    // let result = await User.find({ favTeacher: "ram" });

    //array of object search
    // let result = await User.find({ "favSubject.bookName": "c" });

    //select
    //find has control over object
    //select has control over field
    //in select if all are + don use any - but (it is not valid in _id  )
    // let result = await User.find({}).select("name email age -_id");
    // let result = await User.find({}).select("name email age -isMarried");//dont do like that
    // let result = await User.find({}).select("-name -email -age isMarried "); //dont do like that

    //what ever the position first find,sort, select, skip and limit work

    //regex searching
    //regex searching does not perform exact searching

    let result = await User.find({ name: "nitan" });

    successResponse(res, HttpStatus.OK, "Read User  successfully", result);
  } catch (error) {
    errorResponse(res, HttpStatus.BAD_REQUEST, error.message);
  }
});

export let deleteUser = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await User.findByIdAndDelete(req.params.id);
    successResponse(res, HttpStatus.OK, "Delete User  successfully.", result);
  } catch (error) {
    error.statusCode = HttpStatus.BAD_REQUEST;
    next(error);
  }
});

export let updateUser = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await User.findByIdAndUpdate(req.params.id, req.body);
    successResponse(
      res,
      HttpStatus.CREATED,
      "Update User  successfully.",
      result
    );
  } catch (error) {
    error.statusCode = HttpStatus.BAD_REQUEST;
    next(error);
  }
});
