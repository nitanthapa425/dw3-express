import { HttpStatus } from "../config/constant.js";
import errorResponse from "../helper/errorResponse.js";
import successResponse from "../helper/successResponse.js";
import { Address } from "../schema/model.js";
import expressAsyncHandler from "express-async-handler";

export let createAddress = expressAsyncHandler(async (req, res, next) => {
  let result = await Address.create(req.body);

  successResponse(
    res,
    HttpStatus.CREATED,
    "Address created successfully",
    result
  );
});

export let readAddressDetails = expressAsyncHandler(async (req, res, next) => {
  let result = await Address.findById(req.params.id);
  successResponse(
    res,
    HttpStatus.OK,
    "Read Address details successfully",
    result
  );
});

//get all
//update details (id)
//delete (id)

export let readAllAddress = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await Address.find({});
    successResponse(res, HttpStatus.OK, "Read Address  successfully", result);
  } catch (error) {
    errorResponse(res, HttpStatus.BAD_REQUEST, error.message);
  }
});

export let deleteAddress = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await Address.findByIdAndDelete(req.params.id);
    successResponse(
      res,
      HttpStatus.OK,
      "Delete Address  successfully.",
      result
    );
  } catch (error) {
    error.statusCode = HttpStatus.BAD_REQUEST;
    next(error);
  }
});

export let updateAddress = expressAsyncHandler(async (req, res, next) => {
  try {
    let result = await Address.findByIdAndUpdate(req.params.id, req.body);
    successResponse(
      res,
      HttpStatus.CREATED,
      "Update Address  successfully.",
      result
    );
  } catch (error) {
    error.statusCode = HttpStatus.BAD_REQUEST;
    next(error);
  }
});
