import mongoose from "mongoose";
import { dbUrl } from "../config/constant.js";

let connectDb = async () => {
  let dbPort = dbUrl;

  try {
    await mongoose.connect(dbPort);
    console.log(
      `application is connected to mongodb at port ${dbPort} successfully.`
    );
  } catch (error) {
    console.log(error.message);
  }
};

export default connectDb;
