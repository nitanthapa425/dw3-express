import { Schema } from "mongoose";

// {"name":"",
//  "age":2,
//  "password":3
//   "roll":2,
//    "isMarried":true,
//     "spouseName":"lkjl",
//   "email":"abc@gmail.com",
//   "gender:"male" ,
//   "dob":2019-2-2,
//   "location":{country:"",exactLocation:""},
//   "favTeacher":["a","b","c"],

//   "favSubject":[{bookName:"a",bookAuthor:"a"},{bookName:"b",bookAuthor:"b"},]
//   profileImage:"/.sad.fsd.f//"
// }

export let userSchema = Schema({
  name: {
    type: String,
    // default: "ram",
    // lowercase: true,
    // uppercase:true,
    trim: true,

    required: [true, "name field is required"],
    minLength: [4, "name must be at least 4 character long"],
    maxLength: [20, "name must be at most 20 character"],

    validate: (value) => {
      if (!/^[a-zA-Z0-9\s]+$/.test(value)) {
        throw new Error("Only alphabet and space is allowed.");
      }
    },
  },
  password: {
    type: String,
    // validate: (value) => {
    //   if (
    //     !/^(?=.*[a-z])(?=.*[A-Z])(?=.*\d)(?=.*[@$!%*?&])[A-Za-z\d@$!%*?&]{8,10}$/.test(
    //       value
    //     )
    //   ) {
    //     throw new Error(
    //       "Minimum eight and maximum 10 characters, at least one uppercase letter, one lowercase letter, one number and one special character"
    //     );
    //   }
    // },
  },
  age: {
    type: Number,
    min: [18, "age must be at least 18."],
    max: [50, "age must be at most 50."],
  },
  phoneNumber: {
    type: Number,
    validate: (value) => {
      let strNumber = String(value);
      console.log(strNumber);
      if (strNumber.length !== 10) {
        throw new Error("Phone number must be exact 10 character.");
      }
    },
  },
  isMarried: {
    type: Boolean,
  },
  spouseName: {
    type: String,
    required: [
      function () {
        if (this.isMarried) {
          return true;
        } else {
          this.spouseName = "";
          return false;
        }
      },
      "spouseName is required",
    ],
  },
  email: {
    type: String,
    validate: (value) => {
      if (!/^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/.test(value)) {
        throw new Error("email is wrong.");
      }
    },
  },
  gender: {
    type: String,
    enum: {
      values: ["male", "female", "other"],
      message: (notEnum) => {
        return `${notEnum.value} is not valid enum`;
      },
    },
  },
  dob: {
    type: Date,
  },
  location: {
    country: {
      type: String,
    },
    exactLocation: {
      type: String,
    },
  },
  favTeacher: [
    {
      type: String,
    },
  ],
  favSubject: [
    {
      bookName: {
        type: String,
      },
      bookAuthor: {
        type: String,
      },
    },
  ],

  userProfile: {
    type: String,
  },
});
