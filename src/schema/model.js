import { model } from "mongoose";
import { userSchema } from "./userSchema.js";
import { addressSchema } from "./addressSchema.js";
import { productSchema } from "./productSchema.js";
import { reviewSchema } from "./reviewSchema.js";

export let User = model("User", userSchema);
export let Address = model("Address", addressSchema);
export let Product = model("Product", productSchema);
export let Review = model("Review", reviewSchema);
