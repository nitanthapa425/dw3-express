import { Schema } from "mongoose";

export let addressSchema = Schema({
  city: {
    type: String,
  },
  province: {
    type: Number,
  },
  exactLocation: {
    type: String,
  },
});
