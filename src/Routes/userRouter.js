import { Router } from "express";
import {
  createUser,
  deleteUser,
  readAllUser,
  readUserDetails,
  updateUser,
} from "../controller/userController.js";

let userRouter = Router();

userRouter
  .route("/") //lo.../users
  .post(createUser)
  .get(readAllUser);

userRouter
  .route("/:id")
  .get(readUserDetails)
  .patch(updateUser)
  .delete(deleteUser);
export default userRouter;
