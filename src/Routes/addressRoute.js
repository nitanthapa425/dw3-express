import { Router } from "express";
import {
  createAddress,
  deleteAddress,
  readAddressDetails,
  readAllAddress,
  updateAddress,
} from "../controller/addressController.js";

let addressRouter = Router();

addressRouter
  .route("/") //lo.../addresss
  .post(createAddress)
  .get(readAllAddress);

addressRouter
  .route("/:id")
  .get(readAddressDetails)
  .patch(updateAddress)
  .delete(deleteAddress);
export default addressRouter;
