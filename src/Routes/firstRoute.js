import { Router } from "express";
let firstRouter = Router();

firstRouter
  .route("/admin")
  .get((req, res) => {
    res.json("I am  admin method get");
  })
  .post((req, res) => {
    res.json("I am admin method post");
  })
  .patch((req, res) => {
    res.json("I am admin method patch");
  })
  .delete((req, res) => {
    res.json("I am admin method delete");
  });

firstRouter
  .route("/admin/name")
  .post((req, res) => {
    res.json(" I am admin name method post");
  })
  .get((req, res) => {
    res.json(" I am admin name method get");
  })
  .patch((req, res) => {
    res.json(" I am admin name method patch");
  })
  .delete((req, res) => {
    res.json(" I am admin name method delete");
  });

export default firstRouter;
