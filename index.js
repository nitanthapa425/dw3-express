import express, { json } from "express";
import firstRouter from "./src/Routes/firstRoute.js";
import productRouter from "./src/Routes/productRoute.js";
import connectDb from "./src/connectdb/connectdb.js";
import userRouter from "./src/Routes/userRouter.js";
import errorMiddleware from "./src/middleware/errorMiddleware.js";
import addressRouter from "./src/Routes/addressRoute.js";
import bcrypt from "bcrypt";
import reviewRouter from "./src/Routes/reviewRouter.js";
import jwt from "jsonwebtoken";
import { port, secretKey } from "./src/config/constant.js";
import { generateToken, verifyToken } from "./src/utils/token.js";
import fileRouter from "./src/Routes/fileRouter.js";

let app = express();

app.use(json());

app.use("/a/b", firstRouter);
app.use("/users", userRouter);
app.use("/addresses", addressRouter);
app.use("/products", productRouter);
app.use("/reviews", reviewRouter);
app.use("/files", fileRouter);

connectDb();

app.use(express.static("./public"));
app.use(express.static("./p1"));

// localhost:8000/a.png
// localhost:8000/abcd/report.jpg

app.use(errorMiddleware);

app.listen(port, () => {
  console.log(`app is listening at port ${port}`);
});
